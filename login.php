<?php
$email_usuario = "";
$password_usuario = "";
$usuarios_logeados = array(
    array("camilo@debiandsw.com", ""),
    array("Pedro", ""),
    array("Antonio", ""),
    array("Marcos", ""),
    array("Francisco", "")

);

// comprobamos que estén seteados los campos de usuario y contraseña
// del formulario.
// si es así limpiamos la entrada de datos de coracteres especiales y de espacios en blanco.

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email_usuario = htmlspecialchars($_POST['email']);
    $password_usuario = htmlspecialchars($_POST['password']);
    $email_usuario = trim($email_usuario, "\t\n\r\0\x0B");
    $password_usuario = trim($password_usuario, "\t\n\r\0\x0B");
    $flag_error = true;

    // comprobamos que el Email esté escrito correctamente
    if (!filter_var($email_usuario, FILTER_VALIDATE_EMAIL)) {
        echo ("El email no es válido.");
        $flag_error = false;
    }

//comprobamos que los campos usuario y contraseña no estén vacios

    if (strlen($email_usuario) > 0 && strlen($password_usuario) > 0) {


        $salt = 'XyZzy12*_';
        // este es el email del usuario que tiene permiso para entrar en la base de datos
        $email_logeado = "camilo@debiandsw.com";
        // este es su hash con salt + email + contraseña
        $hash_email_logeado = "e0e21f1f4bee91f7b025dc06728df2d6";

        $cadena_para_hash = $salt . $password_usuario;
        // creamos el hash con salt + email introducido + la contraseña introducida 
        $md5 = hash('md5', $cadena_para_hash);
        // si el hash del usuario que tenemos guardado es igual al hash 
        // calculado con la contraseña y el usuario introducidos en el formulario
        // entonces el usuario queda logeado positivamente
        if ($email_usuario === $email_logeado) {

            if ($hash_email_logeado === $md5) {
                echo ("Usuario logeado con exito");

                error_log("Login success " . $_POST['email']);
                // y lo rederigimos al fichero autos.php con el nombre del usuario
                header("Location: autos.php?name=$email_usuario" . urlencode($_POST[$email_usuario]));
            } else {
                error_log("Login fail" . $_POST["email"] . $hash_email_logeado);
            }
        } else {
            echo ("Email o contraseña incorrecto.");
        }
    } else {

        if (strlen($email_usuario) > 0) {
            echo ("El campo contraseña no puede estar vacío.");
        } else {
            echo ("El campo email no puede estar vacío, se requiere email y contraseña.");
        }
    }
}

?>
<?php
require "vistas/loginview.php";
?>


