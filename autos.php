<?php

// comprobamos si el usuario esta´logeado
$flag_logeado = false;
if (isset($_GET['name'])) {
    if ($_GET['name'] === 'camilo@debiandsw.com') {

        $flag_logeado = true;
    } else {
        die("Falta el parámetro");
    }
} else {
    die("Falta el parámetro");
}

// en caso de estar logeado, instanciamos la clase Auto 
// y creamos un objeto de la clase Auto, y conectamos con 
// la base de datos
if ($flag_logeado == true) {
    require "lib/Database.php";
    require "models/Auto.php";
    $obj_Auto = new Auto();
    $obj_Auto->makeConnection();
}

// comprobamos si se ha hecho click en el botón de borrar, en la lista de autos.
// si es así llamamos a al método delAuto() de la clase Auto y eliminamos ese auto
if (isset($_POST['id1'])) {

    $obj_Auto->delAuto($_POST['id']);
}

// comprobamos si los campos del formulario para la entrada de un nuevo auto
// estám setadas, que no estén vacias y que sean de tipo numérico el año
// y los kilometrajes.

// si es así, añadimos un nuevo auto en la tabla autos de la base de datos
if (isset($_POST['make']) && isset($_POST['year']) && isset($_POST['milage'])) {


    if (strlen($_POST['make']) > 0 && strlen($_POST['year']) > 0 && strlen($_POST['milage']) > 0) {
        if (is_numeric($_POST['year']) && is_numeric($_POST['milage'])) {


            $obj_Auto->setMake($_POST['make']);
            $obj_Auto->setYear($_POST['year']);
            $obj_Auto->setMileage($_POST['milage']);

            $obj_Auto->addAuto($obj_Auto);
        } else {

            echo "</br>";
            echo "Kilometraje y año deben ser numéricos";
        }
    } else {
        echo "</br>";
        echo "Ninguno de los campos pueden estar vacios.";
    }
}



// llamamos a las vistas 

?>

<?php
require "vistas/autosview.php";
?>


